var tugas = '========== No. 1 Looping While ==========';
console.log(tugas)
var loop = 'LOOPING PERTAMA';
console.log(loop)
var flag = 2;
while(flag <= 20) {
    console.log(flag + ' - I love coding');
    flag += 2;
}
console.log()
var loop = 'LOOPING KEDUA';
console.log(loop)
var flag = 20;
while(flag >= 2) {
    console.log(flag + ' - I will become a mobile developer');
    flag -= 2;
}

console.log()

var tugas = '========== No. 2 Looping menggunakan for =========='
console.log(tugas)
for(var angka = 1; angka < 21; angka++) {
    if ( angka %2 != 0 && angka %3 != 0) {
        console.log(angka + ' - Santai')
    } else if ( angka %2 != 1) {
        console.log(angka + ' - Berkualitas')
    } else if ( angka %3 == 
        0 && angka %2 != 0){
        console.log(angka + ' - I Love Coding')
    }
}

console.log()

var tugas = '========== No. 3 Membuat persegi panjang # =========='
console.log(tugas)
var s = '';
for(var i = 0; i < 4; i++) {
    for(var j = 0; j < 8; j++ ) {
        s += '#';
    }   s += '\n'
} console.log(s)

console.log()

console.log()

var tugas = '========== No. 4 Membuat Tangga  =========='
console.log(tugas)
var s = '';
for(var i = 0; i < 7; i++) {
    for(var j = 0; j<= i; j++ ) {
        s += '#';
    }   s += '\n'
} console.log(s)

console.log()

var tugas = '========== No. 5 Membuat Papan Catur  =========='
console.log(tugas)
var s = '';
for(var i = 0; i < 8; i++) {
    for(var j = 0; j < 8; j++ ) {
        if(i %2 == 0) {
            if(j %2 == 0) {
                s += '# '
            } else {
                s += ' '
            }
        } else {
            if(j %2 != 0) {
                s += ' '
            } else {
                s += ' #'
            }
        }
    }   s += '\n'
} console.log(s)