var tugas = '========== NO. 1 =========='
console.log(tugas)
function range(startNum, finishNum) {
    if(finishNum === undefined)
    return -1;

    var arr = [];
    var tambah = startNum < finishNum;

    if(tambah) while(startNum <= finishNum)
    arr.push(startNum++ );

    while(startNum >= finishNum)
    arr.push(startNum--)

    return arr;
}

console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

console.log()

var tugas = '========== NO. 2 =========='
console.log(tugas)
function rangeWithStep(startNum, finishNum, step) {
    if(finishNum === undefined || step === undefined)
    return 1;

    var arr = [];
    var tambah = startNum < finishNum;

    if(tambah) while(startNum <= finishNum)
    arr.push(startNum+= step);

    while(startNum >= finishNum)
    arr.push(startNum-= step)

    return arr;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log()

var tugas = '========== NO. 3 =========='
console.log(tugas)
function sum(startNum, finishNum, step) {
    if(finishNum === undefined || step === undefined)
    return startNum++;

    var arr = [];
    var tambah = startNum < finishNum;

    if(tambah) while(startNum <= step.length)
    arr.push(startNum += finishNum.length);

    while(startNum >= step.length)
    arr.push(startNum -= finishNum.length)

    return arr;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log()

var tugas = '========== NO. 4 =========='
console.log(tugas)
function dataHandling(id,nama,ttl,hobi) {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 

    return input
}