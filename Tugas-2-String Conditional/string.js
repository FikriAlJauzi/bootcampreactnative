// Soal No. 1 (Membuat kalimat)
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var all = `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`

var tugas = 'Output 1'
console.log(tugas);
console.log(all);

console.log()

// Soal No.2 Mengurai kalimat (Akses karakter dalam string)
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.substr(5,6) ; 
var fourthWord = sentence.substr(11, 2) ; 
var fifthWord = sentence.substr(14,2) ;  
var sixthWord = sentence.substr(17,5) ; 
var seventhWord = sentence.substr(23,6) ; 
var eighthWord = sentence.substr(30) ; 

var tugas = 'Output 2'
console.log(tugas);
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

console.log()

// Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14) ;
var thirdWord2 = sentence2.substring(15, 17) ; 
var fourthWord2 = sentence2.substring(18, 20) ; 
var fifthWord2 = sentence2.substring(20, 25) ;  

var tugas = 'Output 3'
console.log(tugas);
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log()

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14) ;
var thirdWord3 = sentence3.substring(15, 17) ; 
var fourthWord3 = sentence3.substring(18, 20) ; 
var fifthWord3 = sentence3.substring(20, 25) ;

var firstWordLength = exampleFirstWord3.length 
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

var tugas = 'Output 4'
console.log(tugas);
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 