var tugas = 'if-else'
console.log(tugas)

// Output untuk Input nama = '' dan peran = ''
var nama = '' 
var peran = ''

if (nama == '') {
    console.log('Nama harus diisi')
} else {
    console.log('Halo John, Pilih peranmu untuk memulai game!')
}

console.log()

//Output untuk Input nama = 'John' dan peran = ''
var nama = 'John' 
var peran = ''

if ( nama == 'John') {
    console.log('Halo John, Pilih peranmu untuk memulai game!')
} else if ( peran == '') {
    console.log('Halo John, Pilih peranmu untuk memulai game!')
} else {
    console.log('Selamat datang di Dunia Werewolf, John')
    console.log('Halo Penyihir John, kamu dapat melihat siapa yang menjadi werewolf!')
}

console.log()

//Output untuk Input nama = 'John' dan peran 'Penyihir'
var nama = 'John' 
var peran = 'Penyihir'

if ( nama == 'John' || peran == 'Penyihir' ) {
    console.log('Selamat datang di Dunia Werewolf, John')
    console.log('Halo Penyihir John, kamu dapat melihat siapa yang menjadi werewolf!')
} else {
    console.log('Nama harus diisi')
}

console.log()

//Output untuk Input nama = 'Jeni' dan peran 'Guard'
var nama = 'Jeni' 
var peran = 'Guard'

if ( nama == 'Jeni' || peran == 'Guard' ) {
    console.log('Selamat datang di Dunia Werewolf, Jeni')
    console.log('Halo Guard Jeni, kamu akan membantu melindungi temanmu dari serangan werewolf.')
} else {
    console.log('Nama harus diisi')
}

console.log()

//Output untuk Input nama = 'Juna' dan peran 'Werewolf'
var nama = 'Juna' 
var peran = 'Werewolf'

if ( nama == 'Juna' || peran == 'Werewolf' ) {
    console.log('Selamat datang di Dunia Werewolf, Juna')
    console.log('Halo Werewolf Juna, Kamu akan memakan mangsa setiap malam!')
} else {
    console.log('Nama harus diisi')
}

console.log()

var tugas = 'Switch Case'
console.log(tugas)

// Kamu akan diberikan sebuah tanggal dalam tiga variabel, yaitu hari, bulan, dan tahun. Disini kamu diminta untuk membuat format tanggal. Misal tanggal yang diberikan adalah hari 1, bulan 5, dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.
// Gunakan switch case untuk kasus ini!
var hari = 21
var bulan = 1
var tahun = 1945

switch(bulan) {
    case 1 :   { console.log(hari + ' ' + 'Januari' + ' ' + tahun); break; }
    case 2 :   { console.log(hari + ' ' + 'Febuari' + ' ' + tahun); break; }
    case 3 :   { console.log(hari + ' ' + 'Maret' + ' ' + tahun); break; }
    case 4 :   { console.log(hari + ' ' + 'April' + ' ' + tahun); break; }
    case 5 :   { console.log(hari + ' ' + 'Mei' + ' ' + tahun); break; }
    case 6 :   { console.log(hari + ' ' + 'Juni' + ' ' + tahun); break; }
    case 7 :   { console.log(hari + ' ' + 'Juli' + ' ' + tahun); break; }
    case 8 :   { console.log(hari + ' ' + 'Agustus' + ' ' + tahun); break; }
    case 9 :   { console.log(hari + ' ' + 'September' + ' ' + tahun); break; }
    case 10:   { console.log(hari + ' ' + 'Oktober' + ' ' + tahun); break; }
    case 11:   { console.log(hari + ' ' + 'November' + ' ' + tahun); break; }
    case 12:   { console.log(hari + ' ' + 'Desember' + ' ' + tahun); }}