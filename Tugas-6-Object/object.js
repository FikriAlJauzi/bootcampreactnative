console.log('========== No. 1 ==========')
const arr =  [
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"],
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023]
]
function arrayToObject(arr) {
    
    let now = new Date()
    let thisYear = now.getFullYear()
    for( let i = 0; i < arr.length; i++) {
        if(arr[i][3] === null || arr[i][3] > thisYear) {
            age = 'Invalid birth year'
        } else {
            age = thisYear - arr[i][3]
        }
        const dataObj = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : age
        }
        console.log(dataObj.firstName + ' ' + dataObj.lastName + ' :' + JSON.stringify(dataObj)) 
    }      
}

arrayToObject(arr)

console.log()

console.log('========== No. 2 ==========')
let barang = [
    ['Sepatu brand Stacattu', 1500000],
    ['Baju brand Zoro seharga', 500000],
    ['Baju brand H&N seharga', 250000],
    ['Sweater brand Uniklooh', 175000],
    ['Casing Handphone', 50000]
]
let buyer = [
    ['1820RzKrnWn0', 2475000,  [ 'Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone' ]],
    ['82Ku8Ma742', 170000, ['Casing Handphone']],
    ['', 2475000, ['Baju brand Zoro seharga']],
    ['234JdhweRxa53', 15000],
    ['']
]
function shoppingTime(memberId, money) {
    let barangLength = barang.length
    for(i = 0; i < barangLength; i++) {
        const memberId = buyer[i]
        let money = parseInt(buyer[i][1])
        let price = parseInt(barang[i][1])
        if(memberId ===null || memberId.length === 0) {
            console.log('Mohon maaf, toko X hanya berlaku untuk member saja');
        } else if(money < barang[i][1]) {
            console.log('Mohon maaf, uang tidak cukup')
        } else if(!memberId ||!money) {
            console.log('Mohon maaf, toko X hanya berlaku untuk member saja')
        }
        const Id = 'memberId: ' + buyer[i][0]
        let buck = 'money: ' + buyer[i][1]
        let listPurchased = 'listPurchased: ' + buyer[i][2]
        let changeMoney = money - price

        console.log(Id)
        console.log(buck)
        console.log(listPurchased)
        console.log('changeMoney: ' + changeMoney)
    }
}
shoppingTime(barang)

console.log()

let objPenumpang = [
    ['pepe', 'A', 'C'],
    ['ramos', 'B', 'F'] ]
console.log('========== No. 3 ==========')
function naikAngkot(objPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    hasil = []
    for(let i = 0; i < rute.length; i++) {

        const dataObj = {
        penumpang : objPenumpang[i],
        naikDari : rute.indexOf(objPenumpang.tujuan),
        tujuan : rute.indexOf(objPenumpang.naikDari),
        bayar : (rute.indexOf(objPenumpang.tujuan) - rute.indexOf(objPenumpang.naikDari)) * 2000
        }
        hasil.push(objPenumpang)
            console.log(JSON.stringify(dataObj))  
    }   
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
[ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]