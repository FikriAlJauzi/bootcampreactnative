console.log('========== No. 1 ==========')
console.log('Release 0')
class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
    set name(x) {
        this._name = x;
    }
    set legs(x) {
        this._legs = x;
    }
    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}
 
var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log()

console.log('Release 1')
class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        console.log('hop hop')
    }
}

class Ape extends Animal {
    constructor(name,x) {
        super(name)
        this._legs = x
    }
    yell() {
        console.log('Auooo')
    }
}
 
var sungokong = new Ape("kera sakti", 2)
sungokong.yell() // "Auooo"
console.log(sungokong.legs)
console.log(sungokong.name)
console.log(sungokong.cold_blooded)
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded) 

console.log()

console.log('========== No. 2 ==========')
class Clock {
    constructor({template}) {
        this.template = template;
    }
    present() {
        let date = new Date();
        let hours = date.getHours();
        if(hours < 10) hours ='0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)

        console.log(output)
        }
        stop() {
            this.clearInterval(timer);
        };
        
        start() {
            this.present();
            this.timer = setInterval(()=>this.present(), 1000);
        };
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  