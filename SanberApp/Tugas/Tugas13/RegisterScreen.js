import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity
} from 'react-native';


export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.tablogo}>
                    <Image source={require('./images/logo.png')} style={{width:375,height:102}} />
                    <Text style={styles.tabTitle}>Login</Text>
                </View>
                <View style={styles.tabAkun}>    
                    <Text style={styles.tabTitleUsername}>Username</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarUsername} />
                    </TouchableOpacity>

                    <Text style={styles.tabTitleEmail}>Email</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarEmail} />
                    </TouchableOpacity>

                    <Text style={styles.tabTitlePass}>Password</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarPass} />
                    </TouchableOpacity>

                    <Text style={styles.tabTitlePassConfrm}>Ulangi Password</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarPassConfrm} />
                    </TouchableOpacity>
                </View>
                <View style={styles.tabLogin}>
                    <TouchableOpacity style={styles.tabBarDaftar}>
                        <Text style={styles.titleDaftar}>Daftar</Text>
                    </TouchableOpacity>

                    <Text style={styles.atau}>atau</Text>                    

                    <TouchableOpacity style={styles.tabBarLogin}>
                        <Text style={styles.titleLogin}>Masuk</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tablogo: {
        alignItems: 'center',
        paddingTop: 30
    },
    tabTitle: {
        fontSize: 30,
        paddingTop: 40
    },
    tabAkun: {
        paddingLeft: 45,
        paddingTop: 30,
        paddingBottom: 40,
        fontSize: 35
    },
    tabBarUsername: {
        marginTop: 4,
        marginBottom: 20,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabEmail: {
        padding: 45,
        fontSize: 35,
    },
    tabBarEmail: {
        marginTop: 4,
        marginBottom: 20,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabPass: {
        paddingLeft: 45,
        paddingTop: 30,
        paddingBottom: 40,
        fontSize: 35
    },
    tabBarPass: {
        marginTop: 4,
        marginBottom: 20,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabPassConfrm: {
        padding: 45,
        fontSize: 35,
    },
    tabBarPassConfrm: {
        marginTop: 4,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabLogin: {
        alignItems: 'center'
    },
    tabBarLogin: {
        width: 120,
        height: 40,
        backgroundColor:'#3EC6FF',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleLogin: {
        fontSize: 25,
        color: 'white'
    },
    tabBarDaftar: {
        width: 120,
        height: 40,
        backgroundColor:'#003366',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleDaftar: {
        fontSize: 25,
        color: 'white'
    },
    atau: {
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10
    }
})           