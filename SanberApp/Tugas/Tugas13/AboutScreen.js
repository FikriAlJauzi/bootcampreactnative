import React, { Component } from 'react';
import { 
    Platform,
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <Text style={styles.titleText}>Tentang Saya</Text>
                        <TouchableOpacity>
                        <Icon style={styles.navItems} name='account-cyrcle' size={170} />
                        </TouchableOpacity>

                        <Text style={styles.name}>Albert Napster</Text>
                        <Text style={styles.occu}>React Native Developer</Text>
                </View>
                <View style={styles.boxPorto}>
                    <Text style={styles.porto}>Portofolio</Text>
                    <FlatList 
                    ItemSeparatorComponent={()=><View style={{height:1,backgroundColor:'#003366'}} />}
                    />
                    <TouchableOpacity style={styles.tabItem} >
                        <Icon name="gitlab" size={45} color="#3EC6FF" /> 
                        <Text style={styles.tabTitle}>Albert Napster</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} >
                        <Icon name='github' size={45} color="#3EC6FF" />
                        <Text style={styles.tabTitle}>Albert Napster</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.boxCont}>
                    <Text style={styles.cont}>Hubungi Saya</Text>
                    <FlatList 
                    ItemSeparatorComponent={()=><View style={{height:1,backgroundColor:'#003366'}} />}
                    />
                    <TouchableOpacity style={styles.tabItem2} >
                        <Icon name='facebook' size={45} color="#3EC6FF" />
                        <Text style={styles.tabTitle2}>Albert Napster</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem2} >
                        <Icon name='instagram' size={45} color="#3EC6FF" />
                        <Text style={styles.tabTitle2}>Albert Napster</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem2} >
                        <Icon name='twitter' size={45} color="#3EC6FF" />
                        <Text style={styles.tabTitle2}>Albert Napster</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    title: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30
    },
    titleText: {
        fontSize: 40,
        color: '#003366',
        fontWeight: 'bold'
    },
    navItems: {
        marginLeft: 25,
        paddingTop: 12
    },
    name: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold',
        paddingTop: 24
    },
    occu: {
        fontSize: 16,
        color: '#3EC6FF',
        paddingTop: 8
    },
    boxPorto: {
        width: 360,
        height: 140,
        backgroundColor:'#EFEFEF',
        marginLeft: 26,
        marginTop: 16,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    porto: {
        fontSize: 18,
        color: '#003366',
        marginTop: 5,
        marginLeft: 78
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 75
    },
    tabTitle: {
        fontSize: 16,
        color: '#003366',
    },
    boxCont: {
        width: 360,
        height: 250,
        backgroundColor:'#EFEFEF',
        marginLeft: 26,
        marginTop: 16,
        borderRadius: 15,
        justifyContent: 'space-around'
    },
    cont: {
        fontSize: 18,
        color: '#003366',
        marginTop: 5,
        marginLeft: 8
    },
    tabItem2: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 5
    },
    tabTitle2: {
        fontSize: 16,
        color: '#003366',
    }
})    