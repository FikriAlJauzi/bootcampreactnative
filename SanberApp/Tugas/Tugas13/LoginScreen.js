import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity
} from 'react-native';


export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.tablogo}>
                    <Image source={require('./images/logo.png')} style={{width:375,height:116}} />
                    <Text style={styles.tabTitle}>Login</Text>
                </View>
                <View style={styles.tabUserEmail}>
                    <Text style={styles.tabTitleUserEmail}>Username / Email</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarUserEmail} />
                    </TouchableOpacity>

                    <Text style={styles.tabTitlePass}>Password</Text>
                    <TouchableOpacity>
                        <View style={styles.tabBarPass} />
                    </TouchableOpacity>
                </View>
                <View style={styles.tabLogin}>
                    <TouchableOpacity style={styles.tabBarLogin}>
                        <Text style={styles.titleLogin}>Masuk</Text>
                    </TouchableOpacity>

                    <Text style={styles.atau}>atau</Text>                    

                    <TouchableOpacity style={styles.tabBarDaftar}>
                        <Text style={styles.titleDaftar}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tablogo: {
        alignItems: 'center',
        paddingTop: 30
    },
    tabTitle: {
        fontSize: 30,
        paddingTop: 40
    },
    tabUserEmail: {
        paddingLeft: 45,
        paddingTop: 30,
        paddingBottom: 40,
        fontSize: 35
    },
    tabBarUserEmail: {
        marginTop: 4,
        marginBottom: 20,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabPass: {
        padding: 45,
        fontSize: 35,
    },
    tabBarPass: {
        marginTop: 4,
        height: 50,
        width: 310,
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor: 'white'
    },
    tabLogin: {
        alignItems: 'center'
    },
    tabBarLogin: {
        width: 120,
        height: 40,
        backgroundColor:'#003366',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleLogin: {
        fontSize: 25,
        color: 'white'
    },
    tabBarDaftar: {
        width: 120,
        height: 40,
        backgroundColor:'#3EC6FF',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleDaftar: {
        fontSize: 25,
        color: 'white'
    },
    atau: {
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10
    }
})           