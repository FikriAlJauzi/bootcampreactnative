console.log('========== No. 1 ==========')
const golden = goldenFunction = () => {
    console.log('this is golden!!')
}

golden();
// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }
   
//   golden()

console.log()

console.log('========== No. 2 ==========')
const newFunction = literal = (firstName,lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
}
newFunction("William", "Imoh").fullName()

console.log()

console.log('========== No. 3 ==========')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation)

console.log()

console.log('========== No. 4 ==========')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west, ...east]
console.log(combinedArray)

console.log()

console.log('========== No. 5 ==========')
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)